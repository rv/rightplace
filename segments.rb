#!/usr/bin/env ruby

# 7 segments
# 5 rows

# r0  ===
# r1  | |
# r2  ===
# r3  | |
# r4  ===

# row 0+2+4
# 0 = ...
# 1 = ===

# row 1+3
# 00 0 = ...
# 01 1 = ..|
# 10 2 = |..
# 11 3 = |.|

# row   0    1    2    3    4    5    6    7    8    9
# 0    ===  ...  ===  ===  ...  ===  ===  ===  ===  ===
# 1    |.|  ..|  ..|  ..|  |.|  |..  |..  ..|  |.|  | |
# 2    ...  ...  ===  ===  ===  ===  ===  ...  ===  ===
# 3    |.|  ..|  |..  ..|  ..|  ..|  |.|  ..|  |.|  ..|
# 4    ===  ...  ===  ===  ...  ===  ===  ...  ===  ===

#   \nr 0 1 2 3 4 5 6 7 8 9
# row\
#  0    1 0 1 1 0 1 1 1 1 1
#  1    3 1 1 1 3 2 2 1 3 3
#  2    0 0 1 1 1 1 1 0 1 1
#  3    3 1 2 1 1 1 3 1 3 1
#  4    1 0 1 1 0 1 1 0 1 1

# segments[0] = [1,3,0,3,1]
# segments[1] = [0,1,0,1,0]
# segments[2] = [1,1,1,2,1]
# segments[3] = [1,1,1,1,1]
# segments[4] = [0,3,1,1,0]
# segments[5] = [1,2,1,1,1]
# segments[6] = [1,2,1,3,1]
# segments[7] = [1,1,0,1,0]
# segments[8] = [1,3,1,3,1]
# segments[9] = [1,3,1,1,1]

# for 24h clock range is 0000 - 2359

# 0000 : 
#   0 = 1111
#   1 = 3333 
#   2 = 0000
#   3 = 3333
#   4 = 1111

# 0001 :
#   0 = 1110
#   1 = 3331 
#   2 = 0000
#   3 = 3331
#   4 = 1110

Segments = []
Segments[0] = [1,3,0,3,1]
Segments[1] = [0,1,0,1,0]
Segments[2] = [1,1,1,2,1]
Segments[3] = [1,1,1,1,1]
Segments[4] = [0,3,1,1,0]
Segments[5] = [1,2,1,1,1]
Segments[6] = [0,2,1,3,1]
Segments[7] = [1,1,0,1,0]
Segments[8] = [1,3,1,3,1]
Segments[9] = [1,3,1,1,1]

def generate_all_4digit_patterns
  patterns = [[],[],[],[],[]]
  for hour in 0..23 do
    h1 = hour.div(10)
    h2 = hour.modulo(10)
    for minute in 0..59 do
      m1 = minute.div(10)
      m2 = minute.modulo(10)
      index = hour * 100 + minute
      for row in 0..4 do
        pattern = "#{Segments[h1][row]}#{Segments[h2][row]}#{Segments[m1][row]}#{Segments[m2][row]}"
        patterns[row].push(pattern)
      end
    end
  end
  return patterns
end

def generate_all_2digit_patterns(max)
  patterns = [[],[],[],[],[]]
  for value in 0..max do
    v1 = value.div(10)
    v2 = value.modulo(10)
    for row in 0..4 do
      pattern = "#{Segments[v1][row]}#{Segments[v2][row]}"
      patterns[row].push(pattern)
    end
  end
  return patterns
end

def generate_4digit_patterns
  allpatterns = generate_all_4digit_patterns
  patterns = [[],[],[],[]]
  for row in 0..4 do
    patterns[row] = allpatterns[row].uniq
    puts "Row #{row} #{allpatterns[row].length} => #{patterns[row].length} patterns"
  end
  return patterns
end

def generate_2digit_patterns(max)
  allpatterns = generate_all_2digit_patterns(max)
  patterns = [[],[],[],[]]
  for row in 0..4 do
    patterns[row] = allpatterns[row].uniq
    puts "Row #{row} #{allpatterns[row].length} => #{patterns[row].length} patterns"
  end
  return patterns
end

def allpresent?(strip, patterns)
  patterns.each do |pattern|
    if not strip.include? pattern
      return false
    end
  end
  return true
end

def find_strip(patterns, values)
  for length in 1..40
    values.repeated_permutation(length) do |p|
      strip = p.join
      if allpresent?(strip, patterns) 
        puts strip
        return strip
      end
    end
    print '.'
  end
end

def find_strips(patterns)
  strips = []
  strips[0] = find_strip(patterns[0], %w(0 1))
  strips[1] = find_strip(patterns[1], %w(1 2 3))
  strips[2] = find_strip(patterns[2], %w(0 1))
  strips[3] = find_strip(patterns[3], %w(1 2 3))
  strips[4] = find_strip(patterns[4], %w(0 1))
  return strips
end

def find_hour_strips
  puts "Hour strips:"
  hour_patterns = generate_2digit_patterns(23)
  strips = find_strips(hour_patterns)
end

def find_minute_strips
  puts "Minute strips:"
  minute_patterns = generate_2digit_patterns(59)
  strips = find_strips(minute_patterns)
end

def find_wide_strips
  puts "Wide strips:"
  patterns = generate_4digit_patterns
  strips = find_strips(patterns)
end

find_hour_strips
find_minute_strips
find_wide_strips

